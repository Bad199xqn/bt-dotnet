﻿using System;
public class Array
{
    static void Main(string[] args)
    {
        int[] arr = new int[10] { 1,3,2,6,5,9,7,2,8, 10 };
        int n = 10;
        Console.WriteLine("Selection sort");
        Console.Write("Input array: ");
        for (int i = 0; i < n; i++)
        {
            Console.Write(arr[i] + " ");
        }
        int temp, smallest;
        for (int i = 0; i < n - 1; i++)
        {
            smallest = i;
            for (int j = i + 1; j < n; j++)
            {
                if (arr[j] < arr[smallest])
                {
                    smallest = j;
                }
            }
            temp = arr[smallest];
            arr[smallest] = arr[i];
            arr[i] = temp;
        }
        Console.WriteLine();
        Console.Write("Sorted array: ");
        for (int i = 0; i < n; i++)
        {
            Console.Write(arr[i] + " ");
        }
    }
}