﻿using System;

namespace MyApplication
{
    class Program
    {
        static void Main(string[] args)
        {
            double X, N, S, factorialNum, exponentialNum;
            Console.Write("X : ");
            X = Convert.ToInt32(Console.ReadLine());
            Console.Write("N : ");
            N = Convert.ToInt32(Console.ReadLine());
            factorialNum = 1;
            exponentialNum = 0;
            S = 1;
            for (int i = 1; i <= N; i++)
            {
                factorialNum = factorialNum * i;
                exponentialNum = Math.Pow(X, i);
                S = S + (factorialNum / exponentialNum);
            }
            Console.Write("S = {0}\n", S);
        }
    }
}