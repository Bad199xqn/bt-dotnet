﻿using System;
public class Exercise11
{
    public static void Main()
    {
        int a, b, c;

        double d, x1, x2;

        Console.Write("\n\n");
        Console.Write("Giai PT ax^2 + bx + c = 0 :\n");
        Console.Write("----------------------------------------");
        Console.Write("\n\n");

        Console.Write("a : ");
        a = Convert.ToInt32(Console.ReadLine());
        Console.Write("b : ");
        b = Convert.ToInt32(Console.ReadLine());
        Console.Write("c : ");
        c = Convert.ToInt32(Console.ReadLine());

        d = b * b - 4 * a * c;
        if (d == 0)
        {
            Console.Write("PT co 2 nghiem bang nhau:\n");
            x1 = -b / (2.0 * a);
            x2 = x1;
            Console.Write("x1= {0}\n", x1);
            Console.Write("x2= {0}\n", x2);
        }
        else if (d > 0)
        {
            Console.Write("PT co 2 nghiem:\n");

            x1 = (-b + Math.Sqrt(d)) / (2 * a);
            x2 = (-b - Math.Sqrt(d)) / (2 * a);

            Console.Write("x1= {0}\n", x1);
            Console.Write("x2= {0}\n", x2);
        }
        else
            Console.Write("PT vo nghiem \n\n");
    }
}
